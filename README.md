# ExampleWebApp

[デモページ](http://example.hy-suzuki.com)

環境構築テスト用のHello Worldを表示するWebアプリです．

- VPS：AWS（Amazon LightSail）
- ドメイン管理：AWS（Route53）
- OS：Ubuntu
- Webサーバ：Apache2（ポートフォワーディング・サブドメイン設定）
- JavaScriptエンジン：Node
- RESTフレームワーク：Express
- CI/CD：GitLab Runner（CI：Dockerベースランナー，CD：shellベースランナー）
- 永続化・監視：systemd・forever
